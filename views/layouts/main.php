<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php

/*Para saber si es dispositivo móvil*/
$esMovil=false;

$useragent=$_SERVER['HTTP_USER_AGENT'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
    $esMovil=true;

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <style>
        iframe{
            overflow: hidden;
            width: 313px;
            height: 153px;
            margin: -31px 0px 0px -18px;
            border: none;
        }
        .colapsed iframe{
            width: 215px;
            height: 83px;
            margin: -10px 0px 0px -11px;
        }
        .navbar-brand{
            width: 291px;
            overflow: hidden;
            height: 107px;
        }
        .navbar-brand.colapsed{
            width: 210px;
            height: 80px;
        }
        .container > .navbar-header{
            color: black;
            overflow: hidden;
        }
    </style>

</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

<?php $this->beginBody() ?>
<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => '<img style="width:100%" src="'.Yii::$app->assetManager->baseUrl.'/../images/logo-texmex.gif"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top text-warning',
            // 'style' => 'background-color:#074D89; margin:0;'
        ],
    ]);
    if(!$esMovil)
        echo '<script type="text/javascript" src="http://www.mega00.com/reproaacp/player.js"></script>

                <script type="text/javascript">
                new WHMSonic({
                path : "http://www.mega00.com/reproaacp/WHMSonic.swf",
                source : "http://198.27.88.63:9976",
                volume : 80,
                autoplay : true,
                width: 300,
                height: 60,
                
                twitter : "",
                facebook : "",
                
                });
                </script>';

    /*Para android*/
    $android = '<a href="rtsp://192.99.9.67:1935/megahosting/radiotexmex"><img src="'.Yii::$app->assetManager->baseUrl.'/../images/android.svg" alt="Android" title="Android" '
        . 'style="vertical-align:super; width:5%;" /></a>';

    /*Para mac*/
    $iphone ='<a href="http://198.27.88.63:9976/listen.pls"><img src="'.Yii::$app->assetManager->baseUrl.'/../images/iphone.svg" alt="iPhone" title="iPhone" '
        . 'style="vertical-align:super; width:5%;"/></a>';

    if(!$esMovil)
        echo $android.$iphone;
    //                echo Html::tag('audio','',array(
    //                    'src'=>'http://198.27.88.63:9976/;',
    //                     'id'=> 'radio',
    //                     'controls'=>'',
    //                     'autoplay'=>true,
    //                     'class'=>'col-md-2 pull-left navbar-nav'));

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right','style'=>'margin-top:0px !important;'],
        'items' => [
            '<li class="hidden"><a href="#page-top"></a></li>',
            $esMovil ? '<li><a href="rtsp://192.99.9.67:1935/megahosting/radiotexmex"><img src="'.Yii::$app->assetManager->baseUrl.'/../images/android.svg" alt="Android" title="Android" '
                . 'style="vertical-align:middle; width:10%; float:left"/> Android</a></li>':'',
            $esMovil ? '<li><a href="http://198.27.88.63:9976/listen.pls"><img src="'.Yii::$app->assetManager->baseUrl.'/../images/iphone.svg" alt="iPhone" title="iPhone" '
                . 'style="vertical-align:middle; width:10%; float:left"/> iPhone</a></li>':'',
            ['label' => 'Que hacemos', 'url' => '#quienes'],
            ['label' => 'Programas', 'url' => '#programas'],
            ['label' => 'Servicios', 'url' => '#servicios'],
            ['label' => 'Anunciantes', 'url' => '#anunciantes'],
            ['label' => 'Contacto', 'url' => '#contactoWeb'],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<!--     <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; My Company </p>
            <p class="pull-right"></p>
        </div>
    </footer> -->
<div class="clearfix visible-xs-block"></div>



<?php $this->endBody() ?>
</body>
<div id="spot-im-root"></div>
<div id="spot-im-root"></div>
<div id="spot-im-root"></div>
<script type="text/javascript">!function(t,o,p){function e(){var t=o.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"==o.location.protocol?"https":"http")+":"+p,o.body.appendChild(t)}t.spotId="889244e05ef4b9a80b219245923f0ba7",t.spotName="",t.allowDesktop=!0,t.allowMobile=!1,t.containerId="spot-im-root",e()}(window.SPOTIM={},document,"//www.spot.im/embed/scripts/launcher.js");</script>
</html>
<?php $this->endPage() ?>

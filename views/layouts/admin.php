<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => '<i class="fa fa-google-wallet"></i> Tex Mex',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top text-warning',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav '],
                'items' => [
                    ['label' => 'Inicio', 'url' => ['/site/index']],
                    ['label' => 'Noticias', 'url' => ['/noticias'],],
                    // ['label' => 'Catalogos', 'items'=>[
                        ['label' => 'Anunciantes', 'url' => ['/anunciantes']],
                        ['label' => 'Usuarios   ', 'url' => ['/usuarios']],
                        ['label' => 'Categorias ', 'url' => ['/categorias']],
                        ['label' => 'Contacto', 'url' => ['/contacto']],
                        ['label' => 'Slider', 'url' => ['/slider']],
                        ['label' => 'Configuraciones', 'url' => ['/usuarios/config']],
                    // ]],
                    Yii::$app->user->isGuest ?['label'=>'']:
                        // ['label' => 'Login', 'url' => ['/site/login']] :
                        ['label' => 'Logout (' . Yii::$app->user->identity->nombre . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">Desarrolado por Globaloxs</p>
            <p class="pull-right"></p>
        </div>
    </footer>
  <div class="clearfix visible-xs-block"></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

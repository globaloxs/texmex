<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin(); ?>
<div class="login-form form-signin">
<h1>Reseteo de contraseña</h1>

    <?= $form->field($model, 'clave')->passwordInput(['maxlength' => 45]) ?>
    <?= $form->field($model, 'clave_repeat')->passwordInput(['maxlength' => 45]) ?>

    <br>
    <div class="form-group">
        <?= Html::submitButton('Cambiar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
use yii\helpers\Html;

 ?>
    <?= Html::beginForm('','post',['enctype' => 'multipart/form-data']); ?>

<div class="login-form form-signin">
<h2>Configuraciones generales</h2>
<div class="panel panel-default body" >
  <div class="panel-heading">Imagen de fondo del sitio</div>
  <div class="panel-body">
    <?= Html::fileInput('imagen'); ?>
  </div>
</div>

    <br>
    <div class="form-group">
        <?= Html::submitButton('Aplicar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= Html::endForm(); ?>


</div>
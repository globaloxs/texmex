<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'usuario')->textInput(['maxlength' => 50]) ?>

    <?= $model->isNewRecord ?$form->field($model, 'clave')->passwordInput(['maxlength' => 45]): ''; ?>
    <?= $model->isNewRecord ?$form->field($model, 'clave_repeat')->passwordInput(['maxlength' => 45]): ''; ?>

    <?= $form->field($model, 'estatus')->dropDownList([ 'ALTA' => 'ALTA', 'BAJA' => 'BAJA', ], []) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar cambios', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
use kartik\popover\PopoverX;
 ?>
<table class="table table-condensed tg table-bordered CSSTableGenerator">
<tr class="a">
<th> </th><th><p>LUNES</p></th><th><p>MARTES</p></th><th><p>MIERCOLES</p></th><th><p>JUEVES</p></th><th><p>VIERNES</p></th><th><p>SABADO</tr>
<tr class="even">

    <td><p>9:00</p></td><td colspan="5" rowspan="6">
        <button type="button" class="btn-link" data-toggle="popover-x" data-placement="top" data-target="#wy2">MUSICA</button>
            <div id="wy2" class="popover popover-default" role="dialog">
            <div class="arrow"></div><div class="popover-title">
            <button type="button" class="close" data-dismiss="popover-x" aria-hidden="true">×</button>
            TexMex
            </div>
            <div class="popover-content">
            <p class="text-justify tg-r221">La mejor selección musical sólo para ti.</p>
            </div>
            </div>
</td>
    <td> </td></tr>
<tr class="a">

    <td><p>10:00</p></td>
    <td>
        <?= 
            PopoverX::widget([
                'content' => "Las mujeres también sabemos ser directas y certeras.<br/><br/>Escucha a este 
                                grupo de amigas que se divierten mientras te invitan a opinar con ellas sobre temas que nos 
                                aquejan e inquietan día a día.  <br/><br/>No te pierdas el espacio creado por mujeres para mujeres… 
                                y para hombres también.",
                'toggleButton' => ['label'=>'VERDADES ALTERNAS', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_LEFT,
                ]);
         ?>
    </td></tr>
<tr class="even">

    <td><p>11:00</p></td>
    <td>        <?= 
            PopoverX::widget([
                'content' => "Viajar sin gastar de más y disfrutando al máximo el destino es posible y sencillo. 
                                Escucha los mejores consejos que todo buen viajero debe tomar en cuenta al momento de hacer la maleta 
                                y cargar la cámara al cuello.<br/><br/>MANUAL DEL VIAJERO te lleva por los mejores destinos nacionales e 
                                internacionales para que sólo te preocupes por decidir dónde será tu siguiente parada.",
                'toggleButton' => ['label'=>'MANUAL DEL VIAJERO', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_LEFT,
                ]);
         ?>
     </td></tr>
<tr class="a">

    <td><p>12:00</p></td>
    <td >
        <?= 
            PopoverX::widget([
                'content' => "Te acercamos a lo mejor y más reciente del séptimo arte: noticias, premieres, soundtracks, 
                                películas clásicas, actores y actrices que se han vuelto leyendas, esto y mucho más, 
                                sólo en DESDE LA BUTACA.<br/><br/>Este espacio es tuyo, disfrútalo y nútrelo con tus anécdotas y recuerdos, 
                                a través de Radio TexMex FM… The Radio With No Limits.  ",
                'toggleButton' => ['label'=>'DESDE LA BUTACA', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_LEFT,
                ]);
         ?>
    </td>


</tr>


<tr class="a">

    <td><p>13:00</p></td>
    <td rowspan="9">
        <?=
        PopoverX::widget([
            'content' => "La mejor selección musical sólo para ti.",
            'toggleButton' => ['label'=>'MUSICA', 'class'=>'btn-link'],
            'placement' => PopoverX::ALIGN_LEFT,
        ]);
        ?>
    </td>

</tr>
<tr class="a">

    <td><p>14:00</p></td>
    </tr>
<tr class="even">

    <td><p>15:00</p></td>
    <td>
        <?= 
            PopoverX::widget([
                'content' => "Buscamos informarte y entretenerte con el  mundo de los caballos y su enorme importancia 
                    en Charrería, Equitación, Equinoterapia e incluso, con consejos médicos para uno de los animales más bellos 
                    del mundo.  
                    The Radio With No Limits. ",
                'toggleButton' => ['label'=>'CHARRERIA', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    <td>
        <?= 
            PopoverX::widget([
                'content' => "Buscamos informarte y entretenerte con el  mundo de los caballos y su enorme importancia en Charrería, 
                Equitación, Equinoterapia e incluso, con consejos médicos para uno de los animales más bellos del mundo.",
                'toggleButton' => ['label'=>'EQUITACION', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    <td>
        <?= 
            PopoverX::widget([
                'content' => "Buscamos informarte y entretenerte con el  mundo de los caballos y su enorme importancia en Charrería, Equitación, 
                Equinoterapia e incluso, con consejos médicos para uno de los animales más bellos del mundo.",
                'toggleButton' => ['label'=>'EQUINOTERAPIA', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    <td>        <?= 
            PopoverX::widget([
                'content' => "Buscamos informarte y entretenerte con el  mundo de los caballos y su enorme importancia en Charrería, Equitación, 
                Equinoterapia e incluso, con consejos médicos para uno de los animales más bellos del mundo.",
                'toggleButton' => ['label'=>'CHARRERIA', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
     </td>
    <td>
        <?= 
            PopoverX::widget([
                'content' => "Buscamos informarte y entretenerte con el  mundo de los caballos y su enorme importancia en Charrería, Equitación, 
                Equinoterapia e incluso, con consejos médicos para uno de los animales más bellos del mundo.",
                'toggleButton' => ['label'=>'CORSEL', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>

</tr>
<tr class="a">

    <td><p>16:00</p></td><td colspan="5">        
    <?= 
            PopoverX::widget([
                'content' => "De lunes a viernes en punto de las 16:00 horas, te encargas de programar Radio TexMex FM. 
                Comunícate con nosotros, pide tu canción favorita, dedícala o manda saludos y ¡listo! Lo pides, lo escuchas.
                <br/><br/>RADIO TEXMEX A LA CARTA son las complacencias en Radio TexMex FM… The Radio With No Limits",
                'toggleButton' => ['label'=>'RADIO TEXMEX A LA CARTA', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_LEFT,
                ]);
         ?>
     </td>
    </tr>
<tr class="even">

    <td><p>17:00</p></td>
    <td>
        <?= 
        PopoverX::widget([
            'content' => "Te presentamos un espacio que todos necesitamos pero nadie lo trata como nosotros. 
            Te damos consejos prácticos y útiles sobre diferentes áreas de interés que te serán de valiosa ayuda. 
            Profesionales de su ramo hablando en un lenguaje claro, sencillo y amigable.<br/>
            <br/>Sintonízanos de lunes a viernes, de 17:00 a 18:00 horas, todos días con un tema diferente",
            'toggleButton' => ['label'=>'PARA SABER MAS: FISCAL', 'class'=>'btn-link'],
            'placement' => PopoverX::ALIGN_TOP,
            ]);
        ?>
    </td>    
    <td>
        <?= 
        PopoverX::widget([
            'content' => "Te presentamos un espacio que todos necesitamos pero nadie lo trata como nosotros. 
            Te damos consejos prácticos y útiles sobre diferentes áreas de interés que te serán de valiosa ayuda. 
            Profesionales de su ramo hablando en un lenguaje claro, sencillo y amigable.<br/>
            <br/>Sintonízanos de lunes a viernes, de 17:00 a 18:00 horas, todos días con un tema diferente",
            'toggleButton' => ['label'=>'PARA SABER MAS: LEGAL', 'class'=>'btn-link'],
            'placement' => PopoverX::ALIGN_TOP,
            ]);
        ?>
    </td>    
    <td>
        <?= 
        PopoverX::widget([
            'content' => "Te presentamos un espacio que todos necesitamos pero nadie lo trata como nosotros. 
            Te damos consejos prácticos y útiles sobre diferentes áreas de interés que te serán de valiosa ayuda. 
            Profesionales de su ramo hablando en un lenguaje claro, sencillo y amigable.<br/>
            <br/>Sintonízanos de lunes a viernes, de 17:00 a 18:00 horas, todos días con un tema diferente",
            'toggleButton' => ['label'=>'PARA SABER MAS: MEDICA', 'class'=>'btn-link'],
            'placement' => PopoverX::ALIGN_TOP,
            ]);
        ?>
    </td>    
    <td>
        <?= 
        PopoverX::widget([
            'content' => "Te presentamos un espacio que todos necesitamos pero nadie lo trata como nosotros. 
            Te damos consejos prácticos y útiles sobre diferentes áreas de interés que te serán de valiosa ayuda. 
            Profesionales de su ramo hablando en un lenguaje claro, sencillo y amigable.<br/>
            <br/>Sintonízanos de lunes a viernes, de 17:00 a 18:00 horas, todos días con un tema diferente",
            'toggleButton' => ['label'=>'PARA SABER MAS: TECNOLOGIA', 'class'=>'btn-link'],
            'placement' => PopoverX::ALIGN_TOP,
            ]);
        ?>
    </td>    
    <td>
        <?= 
        PopoverX::widget([
            'content' => "Te presentamos un espacio que todos necesitamos pero nadie lo trata como nosotros. 
            Te damos consejos prácticos y útiles sobre diferentes áreas de interés que te serán de valiosa ayuda. 
            Profesionales de su ramo hablando en un lenguaje claro, sencillo y amigable.<br/>
            <br/>Sintonízanos de lunes a viernes, de 17:00 a 18:00 horas, todos días con un tema diferente",
            'toggleButton' => ['label'=>'PARA SABER MAS: AGROINDUSTRIAL', 'class'=>'btn-link'],
            'placement' => PopoverX::ALIGN_TOP,
            ]);
        ?>
    </td>    
        </tr>
<tr class="a">

    <td><p>18:00</p></td>
    <td colspan="5">
    <?= 
            PopoverX::widget([
                'content' => "Hacemos que las distancias desaparezcan y te sientas más cerca que nunca de los tuyos. 
                SIN FRONTERAS, MÉXICO – ESTADOS UNIDOS, se convierte en el espacio que estabas esperando para estar en 
                contacto con tu familia y amigos en Estados Unidos o bien en México. <br/><br/>Información, Orientación, 
                Consejos, Saludos, Peticiones. Todo lo relacionado con nuestros paisanos en la Unión Americana está aquí.<br/>
                <br/>Escucha SIN FRONTERAS, MÉXICO – ESTADOS UNIDOS de lunes de a viernes a partir de 18:00 horas, 
                sólo en Radio TexMex FM… The Radio With No Limits. ",
                'toggleButton' => ['label'=>'SIN FRONTERAS MÉXICO-ESTADOS UNIDOS', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    </tr>
<tr class="even">

    <td><p>19:00</p></td>
    <td>
    <?= 
            PopoverX::widget([
                'content' => "El mundo de los autos al alcance de tu mano. Un enlace directo con lo mejor del mundo 
                del automovilismo y motociclismo, comentado por expertos y profesionales.",
                'toggleButton' => ['label'=>'AUTOS: KARTISMO', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    <td>
    <?= 
            PopoverX::widget([
                'content' => "El mundo de los autos al alcance de tu mano. Un enlace directo con lo mejor del mundo 
                del automovilismo y motociclismo, comentado por expertos y profesionales.",
                'toggleButton' => ['label'=>'AUTOS: AUTOMOVILISMO F1', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    <td>
    <?= 
            PopoverX::widget([
                'content' => "El mundo de los autos al alcance de tu mano. Un enlace directo con lo mejor del mundo 
                del automovilismo y motociclismo, comentado por expertos y profesionales.",
                'toggleButton' => ['label'=>'AUTOS: RALLY', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    <td>
    <?= 
            PopoverX::widget([
                'content' => "El mundo de los autos al alcance de tu mano. Un enlace directo con lo mejor del mundo 
                del automovilismo y motociclismo, comentado por expertos y profesionales.",
                'toggleButton' => ['label'=>'MOTOCICLISMO', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    <td>
    <?= 
            PopoverX::widget([
                'content' => "El mundo de los autos al alcance de tu mano. Un enlace directo con lo mejor del mundo 
                del automovilismo y motociclismo, comentado por expertos y profesionales.",
                'toggleButton' => ['label'=>'AUTOS: PISTA', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    </tr>
<tr class="even">

    <td><p>20:00</p></td><td colspan="5">
    <?= 
            PopoverX::widget([
                'content' => "La enseñanza no debe estar peleada con la diversión… Aprende de sexualidad mientras te 
                diviertes o al revés, diviértete y aprende uno que otro consejo para disfrutar mucho más.<br/><br/>Tus dudas, 
                fantasías e inquietudes ya tienen un lugar donde el tabú se queda afuera y la mente abierta es nuestra protagonista. 
                <br/><br/>Escucha DIVERTIDÍSIMO RADIO TEMÁTICO de lunes a viernes, en punto de las 20:00 horas. En exclusiva por Radio
                 TexMex FM… The Radio With No Limits.",
                'toggleButton' => ['label'=>'DIVERTIDISIMO RADIO TEMATICO', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    </tr>
<tr class="a">

    <td><p>21:00</p></td><td colspan="5">
    <?= 
            PopoverX::widget([
                'content' => "La mejor selección musical sólo para ti.",
                'toggleButton' => ['label'=>'MUSICA', 'class'=>'btn-link'],
                'placement' => PopoverX::ALIGN_TOP,
                ]);
         ?>
    </td>
    </tr>

</table>

<style type="text/css">
    .tg tr >td{
        font-size: 10px;
        text-align: center;
        vertical-align: middle !important;
    }
    .tg tr >th{
        background: #036 !important;
        color:#FFF;
    }
        .tg tr > td:first-child{background:#ccc;}

</style>
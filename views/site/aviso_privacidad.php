<?php 
use yii\helpers\Markdown;
 ?>
<style type="text/css">
    body{
        background:0 ; 
        padding:0;
    }
    .wrap > .container{padding:0px;}
    img{margin: 10px auto; }
</style>
<div class="content section" style="background:#000;vertical-align:middle;text-align:center" >
    <img src="images/logo_texmex.svg">
    </div>
 <?=
Markdown::process("
#CONDICIONES DE USO DE LA WEB TEXMEX.COM.MX

##I.- Condiciones de uso. 
TEXMEX.COM.MX es una web cuya titularidad pertenece a TEXMEX S.A. DE C.V., con domicilio en calle Avenida 31 Oriente 207, interior 2. Colonia el Carmen Huexotitla. Puebla, Puebla.
En un principio, la utilización de la web es de carácter gratuito, y en algún caso, dicha utilización podría estar sujeta a Condiciones Generales y Condiciones Particulares de utilización. Por ello, el acceso y utilización de los servicios de la web significará la aceptación de dichas Condiciones.

##II.- Contenido, términos legales y responsabilidades. 
La Web contiene imágenes e información relacionada a las actividades de la empresa y otros contenidos elaborados por el titular de la misma y sus colaboradores, los cuales tienen fines meramente informativos, ya que en alguna ocasión, éstos podrían no reflejar la actualidad más reciente, por lo que en cualquier momento dichos contenidos podrán ser modificados y/o sustituidos por unos nuevos, sin notificación previa, ni responsabilidad alguna.

Los Contenidos de la web no pueden ser considerados, en ningún caso, como sustitutivos de asesoramiento legal, ni de ningún otro tipo de asesoramiento. No existirá ningún tipo de relación comercial, profesional, ni ningún tipo de relación de otra índole con los profesionales que integran la web por el mero hecho del acceso a ella por parte de los usuarios.
Para el caso de existir enlaces con distintas páginas web, TEXMEX.COM.MX, no se hará responsable de su contenido al carecer de control sobre las mismas.
El usuario cuando accede a la web, lo hace por su propia cuenta y riesgo. TEXMEX.COM.MX no garantiza ni la rapidez, ni la ininterrupción, ni la ausencia de virus en la web. Asimismo, el titular de la web, los colaboradores, sus socios, empleados y representantes tampoco podrán ser considerados responsables por cualesquiera daños derivados de la utilización de esta web, ni por cualquier actuación realizada sobre la base de la información que en ella se facilita.
No se garantiza la ausencia de virus u otros elementos lesivos que pudieran causar daños o alteraciones en el sistema informático, en los documentos electrónicos o en los archivos del usuario de este sitio web. En consecuencia, no se responde por los daños y perjuicios que tales elementos pudieran ocasionar al usuario o a terceros.

##III.- Propiedad Intelectual e Industrial. 
Se autoriza el almacenamiento y reproducción temporal de alguno o algunos de los Contenidos de la web, con el propósito de ser utilizados con un fin personal y no comercial. La reproducción, el almacenamiento permanente y la difusión de los Contenidos de la web o cualquier uso que tenga fines públicos o comerciales queda terminantemente prohibido sin el previo consentimiento por escrito del titular de la web, y siempre que se haga referencia explícita a TEXMEX.COM.MX y a su titularidad.

##IV.- Protección de datos de carácter personal. 
Toda aquella información recibida en la web, a través de consultas, envío de datos, o cualquier otro medio, será tratada con la más estricta y absoluta confidencialidad.
Los datos de carácter personal que se hayan obtenido a través de la web, tendrán la protección necesaria a fin de evitar su alteración, pérdida, tratamiento o acceso no autorizados.
Si usted desea ver o modificar su información almacenada en TEXMEX.COM.MX, debe de escribir al email: conacto@TEXMEX.COM.MX con el titulo “información privada”.

    ");
  ?>
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

?>
<div class="site-contact">
    

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <div class="alert alert-success">
        Gracias por contactarnos. Te responderemos en cuanto sea posible.        
    </div>

    <p>
<!--        Note that if you turn on the Yii debugger, you should be able
        to view the mail message on the mail panel of the debugger.-->
        <?php if (Yii::$app->mailer->useFileTransport): ?>
        Because the application is in development mode, the email is not sent but saved as
        a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
        Please configure the <code>useFileTransport</code> property of the <code>mail</code>
        application component to be false to enable email sending.
        <?php endif; ?>
    </p>

    <?php endif; ?>   
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                            'id' => 'contact-form',
                            'action' => Url::to('#contactoWeb')
                    ]); ?>
                <?= $form->field($model, 'name') ?>
                
                <?= $form->field($model, 'email',['template' => '
                        <label for "email">{label}</label>
                        
                            <div class="input-group  ">
                               <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-envelope"></span>
                               </span>
                               {input}
                        </div>
                            {error}{hint}
                        '])->textInput(['placeholder' => 'Ingrese su correo electronico'])  ?>
                
                <?= $form->field($model, 'subject')->dropDownList(
                                                        [
                                                            'Opiniones'=>'Opiniones',
                                                            'Sugerencias'=>'Sugerencias',
                                                            'Preguntas'=>'Preguntas'
                                                        ], 
                                                        [
                                                            'prompt'=>'-Seleccione una-']
                                                    ) ?>    
                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="">{image}</div><div >{input}</div></div>',
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Enviar mensaje', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    
</div>

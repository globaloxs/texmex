<?php
/* @var $this yii\web\View */
$this->title = 'TexMex Radio';
use yii\web\View;
use yii\helpers\Html;
use kartik\social\TwitterPlugin;
use kartik\social\FacebookPlugin;
use yii\grid\GridView;
use kartik\popover\PopoverX;
use yii\helpers\Markdown;
use yii\base\Controller;
use app\models\ContactForm;

?>
    <!-- ******HEADER****** --> 
<style>
#programa-imagen {
   width: 400px;
   
}
</style>
    
    <div class="container sections-wrapper">

        <div class="row">
               <div class="col-md-12 section">
                    <div class="section-banner">
                        <!-- <h2 class="heading">Algunos de nuestros proveedores</h2> -->
                        <div class="content">    
                        <?= $this->render('/slider/widget'); ?>                          
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </div><!--//section-->
                
            <div class="primary col-md-8 col-sm-12 col-xs-12">
                <section class="about section" id="quienes">
                    <div class="section-inner">
                        <h2 class="heading">Nuestra programacion</h2>
                        <div class="content">
                            <?= $this->render('/site/_programacion');  ?>
                        </div><!--//content-->
                    </div><!--//section-inner-->                 
                </section><!--//section-->
    

                
                <section class="projects section" id="programas">
                    <div class="section-inner">
                        <h2 class="heading" >Programas</h2>
                        <div class="content">
                                 <div class="row">
                                    <div class="col-md-6" id="lista-programas">

                                        <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#programas" data-pid="0">
                                                <img class="img-responsive" src="images/programas/de-caballos.gif" alt="De caballos y..." >
                                            </a>
                                        </div>                                        
                                        <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#programas" data-pid="2">
                                                <img class="img-responsive" src="images/programas/para-saber.gif" alt="Para saber" >
                                            </a>
                                        </div>
                                        
                                        <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#programas" data-pid="4">
                                                <img class="img-responsive" src="images/programas/de-autos.gif" alt="De autos" >
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#programas" data-pid="5">
                                                <img class="img-responsive" src="images/programas/divertidisimo.gif" alt="Divertidisimo" >
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#programas" data-pid="6">
                                                <img class="img-responsive" src="images/programas/verdades-alternas.gif" alt="Verdades Alternas" >
                                            </a>
                                        </div>                                                                               
                                         <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#programas" data-pid="8">
                                                <img class="img-responsive" src="images/programas/desde-butaca.gif" alt="desde-butaca" >
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#programas" data-pid="7">
                                                <img class="img-responsive" src="images/programas/manual-viajero.gif" alt="Manual del Viajero" >
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#locutores" data-pid="1">
                                                <img class="img-responsive" src="images/programas/a-la-carta.gif" alt="Texmex a la Carta" >
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail btn-programa" href="#locutores" data-pid="3">
                                                <img class="img-responsive" src="images/programas/migrante.gif" alt="Sin fronteras, México - EU" >
                                            </a>
                                        </div>



                                    </div>
                                    <div class="col-md-6 text-center col-xs-6">
                                        <div class="col-lg-9 col-center ">
                                                <img id="programa-imagen" class="img-responsive" src="images/programas/a-la-carta.gif" alt="">
                                                <h3 id="programa-nombre">Selecciona un programa</h3>
                                        </div>
                                            <div class="highlight text-justify">
                                                <div id="programa-descripcion"></div>
                                            </div>
                                    </div>
                                </div>


                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </section><!--//section-->

                <section class="experience section" id="servicios">
                    <div class="section-inner">
                        <h2 class="heading" >Servicios</h2>
                        <div class="content row">
 <?=
 // $parser = new \yii\console\Markdown();
// $parser->renderCode("
 Markdown::process("
En Radio TexMex FM queremos que te escuchen, te vean, te sigan, te encuentren en todos lados. Para lograrlo, hemos desarrollado los mejores servicios para tu marca o empresa y a continuación los detallamos.

####RADIO TEXMEX FM:
<img src='images/3269308813_cbc7a9f734_m.jpg' class='pull-right img-rounded'/>
*    Impactos de Spots durante la programación.

*    Entrevista

*   Mención en Vivo

*    Control Remoto

*    Patrocinio

*    Product Placement

####PÁGINA OFICIAL Y REDES SOCIALES:
<img src='images/8464661409_32aa7a26a6_m.jpg' class='pull-right img-rounded'/>

* Banner

* Product Placement 

####PUBLICIDAD MÓVIL:

* Vallas Móviles

* Perifoneo

* Lonas Publicitarias

####AGENCIA DE PUBLICIDAD:
<img src='images/5005035816_9cfc9bf6a8_m.jpg' class='pull-right img-responsive img-thumbnail'/>

* Realización de Spots (Preproducción, Producción y Post Producción)

* Campañas de Publicidad

**Contáctanos y comienza a disfrutar los beneficios de anunciarte con Radio TexMex FM… The Radio With No Limits.**");
  ?>
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </section><!--//section-->
                
                <section class="experience section" id="anunciantes">
                    <div class="section-inner">
                        <h2 class="heading" >Anunciantes</h2>
                        <div class="content row">
                            <?= $this->render('/anunciantes/widget'); ?>
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </section><!--//section-->

                <section class="github section" id="contacto">




                    <div class="section-inner">


                        <h2 class="heading">RADIO TEXMEX FM… La Radio sin Fronteras</h2>
                        <h4>CONCEPTO:</h4>
                        <p>Radio TexMex FM representa un enlace entre los oyentes mexicanos que radican en el país y los radican en Estados Unidos, brindándoles un espacio donde, a lo largo del día, escuchan música regional mexicana y pop en español, así como programas de su interés, aunado a las mejores campañas de publicidad y promoción.</p>
                        <div><br/></div>

                        <h4>MERCADO META:</h4>
                        <p>Aunque Radio TexMex FM nace en Puebla, México, es una empresa radiofónica por internet que busca expandirse más allá de las fronteras mexicanas, sobre todo donde radican mexicanos, un claro ejemplo es la Unión Americana. Nos dirigimos sobre todo a gente que trabaja, así como a gente que se encuentra estudiando el grado superior y/o posgrados. Nuestra captación es principalmente una audiencia C, B y B+, es decir, media baja, media y media alta, de 25 a 40 años, con un perfil 50% hombres, 50% mujeres.</p>
                        <div><br/></div>


                        <h4>MISIÓN:</h4>
                        <p>Somos una empresa innovadora en los medios de comunicación actuales. Ofrecemos una opción diferente, humana, real y abierta para los radioescuchas con contenidos de su total interés, además de brindar soporte para quienes buscan publicitarse de una forma completa y competitiva en el mercado mexicano y norteamericano.
                            <br/><br/>Radio TexMex FM es el puente de comunicación entre el mercado y mexicano e hispano en Estados Unidos.</p>
                        <div><br/></div>

                        <h4>VISIÓN:</h4>
                        <p>Convertirnos en la mejor empresa radiofónica no sólo en el mercado de estaciones de radio en internet, también de los espacios en radio al aire en frecuencia modulada. Seremos quien proteja de manera leal los intereses de nuestros clientes sin renunciar jamás a la esencia de nuestra filosofía: complacer a nuestros radioescuchas en base a sus gustos y necesidades. </p>
                        <div><br/></div>

                        <h4>VALORES:</h4>
                        <p>
                            <ul>
                                <li><p>Responsabilidad</p></li>
                                <li><p>Calidad</p></li>
                                <li><p>Calidez</p></li>
                                <li><p>Honestidad</p></li>
                                <li><p>Trabajo en equipo</p></li>
                                <li><p>Libertad</p></li>
                            </ul>
                        </p>
                        <div><br/></div>

                        <h2 class="heading" id="contactoWeb">Contacto</h2>
                        <div class="row" >
                               <!--inicio de formulario de contacto-->
                                <?php 
                                $model = new ContactForm();
                                if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
                                    Yii::$app->session->setFlash('contactFormSubmitted');

                                    //return $this->refresh();
                                }
                                    echo $this->render('contact', [
                                        'model' => $model,
                                    ]);
                                


                                ?>
                        </div>
<div class="container">
        <br>

</div>

                        
                    </div><!--//secton-inner-->
                </section><!--//section-->
            </div><!--//primary-->
            <div class="secondary col-md-4 col-sm-12 col-xs-12">

                
                <aside class="skills aside section">
                    <div class="section-inner">
                        <h2 class="heading">Noticias</h2>
                        <div class="content">
                            <?= $this->render('/noticias/widget'); ?>                          
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->

                
                <aside class="education aside section">
                    <div class="section-inner">
                        <h2 class="heading">Redes sociales</h2>
                        <div class="content text-center">
                            <?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::LIKE_BOX, 'settings' => ['href'=>'http://facebook.com/FacebookDevelopers']]); ?>
                            <br>
                            <a class="twitter-timeline" href="https://twitter.com/radiotexmex" data-widget-id="573158499489730560">Tweets by @radiotexmex</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                            <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
                            <script src="https://apis.google.com/js/platform.js" async defer>
                              {lang: 'es'}
                          </script>
                          <h3>Instagram</h3>
                          <!-- www.intagme.com -->
                          <iframe src="http://www.intagme.com/in/?h=bWlncmFudGV8c2x8MzAwfDJ8M3x8eWVzfDV8dW5kZWZpbmVkfHllcw==" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:315px; height: 315px" ></iframe>

<!--  -->                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                <aside class="skills aside section">
                    <div class="section-inner">
                        <h2 class="heading">Noticias</h2>
                        <div class="content">
<iframe src="http://www.eluniversal.com.mx/widgets/videos.html?t=v&w=290&h=460&s=canalDestacados" width="300" height="470" scrolling="no" frameborder="0"></iframe>                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                            
                 <aside class="info aside section">
                    <div class="section-inner">
                        <h2 class="heading " id="clima">Clima</h2>
                        <div class="content">
                            <a href="http://www.accuweather.com/en/mx/puebla/245020/weather-forecast/245020" class="aw-widget-legal">
                            <!--
                            By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
                            -->
                            </a><div id="awcc1416326855272" class="aw-widget-current"  data-locationkey="245020" data-unit="c" data-language="en-us" data-useip="true" data-uid="awcc1416326855272"></div><script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </aside><!--//aside-->

<!--
                
                <aside class="credits aside section">
                    <div class="section-inner">
                        <!-- <h2 class="heading">Nuestros datos</h2> -->
                        <div class="content">
                                <form>
                                    <legend><span class="glyphicon glyphicon-globe"></span> Nuestras oficinas.</legend>
                                    <address>
                                        <strong>Radio Tex Mex.</strong><br>
                                        Avenida 31 Oriente 207, interior 102.
                                        Colonia el Carmen Huexotitla.  <br>
                                        Puebla, Puebla.<br>
                                        <abbr title="Phone">
                                            Tel:</abbr>
                                            (222) 211-3984
                                        </address>
                                        <address>
                                            <strong>Correo electronico</strong><br>
                                            <a href="mailto:contacto@radiotexmex.com.mx">contacto@radiotexmex.com.mx</a>
                                        </address>
                                    </form>
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
            </div><!--//secondary-->    
        </div><!--//row-->
    </div><!--//masonry-->
    
    <!-- ******FOOTER****** --> 
    <footer class="footer">
        <div class="container text-center">
                <small class="copyright">Diseñado por <a href="http://globaloxs.com" target="_blank">Globaloxs</a> . 
<?= Html::a('Aviso de privacidad',array('site/aviso'),array('target'=>'_blank'));?>
                </small>
        </div><!--//container-->
    </footer><!--//footer-->
<!--Start of Zopim Live Chat Script
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2muTvcx5F2g6K74vp4m9if0a6YETUi3E";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");


window.setTimeout(function() {
    console.log("abrir chat");
    $zopim.livechat.window.show();
    }, 1000); //time in milliseconds

-->

<?php 
$this->registerJs('
    


function aud_play_pause() {
  var myAudio = document.getElementById("radio");
  if (myAudio.paused) {
    myAudio.play();
  } else {
    myAudio.pause();
  }
}
    var programas=[
    {"nombre":" DE CABALLOS Y…","imagen":"images/programas/de-caballos.gif", "descripcion":"Buscamos informarte y entretenerte con el  mundo de los caballos y su enorme importancia en Charrería, Equitación, Equinoterapia e incluso, con consejos médicos para uno de los animales más bellos del mundo.  <br/><br/>Escúchanos de lunes a viernes de 15:00 a 16:00 horas por Radio TexMex Fm… The Radio With No Limits. "},
    {"nombre":" RADIO TEXMEX A LA CARTA","imagen":"images/programas/a-la-carta.gif", "descripcion":"De lunes a viernes en punto de las 16:00 horas, te encargas de programar Radio TexMex FM. Comunícate con nosotros, pide tu canción favorita, dedícala o manda saludos y ¡listo! Lo pides, lo escuchas.<br/><br/>RADIO TEXMEX A LA CARTA son las complacencias en Radio TexMex FM… The Radio With No Limits "},
    {"nombre":" PARA SABER MÁS…", "imagen":"images/programas/para-saber.gif", "descripcion":"Te presentamos un espacio que todos necesitamos pero nadie lo trata como nosotros. Te damos consejos prácticos y útiles sobre diferentes áreas de interés que te serán de valiosa ayuda. Profesionales de su ramo hablando en un lenguaje claro, sencillo y amigable.<br/><br/>Sintonízanos de lunes a viernes, de 17:00 a 18:00 horas, todos días con un tema diferente:<br/><br/>Lunes: Para Saber Más – Asesoría Fiscal<br/><br/>Martes: Para Saber Más – Asesoría Legal<br/><br/>Miércoles: Para Saber Más – Asesoría Médica<br/><br/>Jueves: Para Saber Más – Asesoría Tecnológica<br/><br/>Viernes: Para Saber Más – Asesoría Agroindustrial <br/><br/>¡Hacemos tu vida más fácil! Radio TexMex FM… The Radio With No Limits "},
    {"nombre":" SIN FRONTERAS, MÉXICO – ESTADOS UNIDOS","imagen":"images/programas/migrante.gif", "descripcion":"Hacemos que las distancias desaparezcan y te sientas más cerca que nunca de los tuyos. SIN FRONTERAS, MÉXICO – ESTADOS UNIDOS, se convierte en el espacio que estabas esperando para estar en contacto con tu familia y amigos en Estados Unidos o bien en México. <br/><br/>Información, Orientación, Consejos, Saludos, Peticiones. Todo lo relacionado con nuestros paisanos en la Unión Americana está aquí.<br/><br/>Escucha SIN FRONTERAS, MÉXICO – ESTADOS UNIDOS de lunes de a viernes a partir de 18:00 horas, sólo en Radio TexMex FM… The Radio With No Limits. "},
    {"nombre":" DE AUTOS Y…", "imagen":"images/programas/de-autos.gif", "descripcion":"El mundo de los autos al alcance de tu mano. Un enlace directo con lo mejor del mundo del automovilismo y motociclismo, comentado por expertos y profesionales.<br/><br/>Lunes: De Autos Y… Kartismo<br/><br/>Martes: De Autos Y… Automovilismo F1<br/><br/>Miércoles: De Autos Y… Rally<br/><br/>Jueves: De Autos Y… Motociclismo<br/><br/>Viernes: De Autos Y… Pista<br/><br/>No nos pierdas la pista de lunes a viernes de 19:00 a 20:00 horas, a través de Radio TexMex FM… The Radio With No Limits. "},
    {"nombre":" DIVERTIDÍSIMO RADIO TEMÁTICO", "imagen":"images/programas/divertidisimo.gif", "descripcion":"La enseñanza no debe estar peleada con la diversión… Aprende de sexualidad mientras te diviertes o al revés, diviértete y aprende uno que otro consejo para disfrutar mucho más.<br/><br/>Tus dudas, fantasías e inquietudes ya tienen un lugar donde el tabú se queda afuera y la mente abierta es nuestra protagonista. <br/><br/>Escucha DIVERTIDÍSIMO RADIO TEMÁTICO de lunes a viernes, en punto de las 20:00 horas. En exclusiva por Radio TexMex FM… The Radio With No Limits. "},
    {"nombre":" VERDADES ALTERNAS", "imagen":"images/programas/verdades-alternas.gif", "descripcion":"Las mujeres también sabemos ser directas y certeras.<br/><br/>Escucha a este grupo de amigas que se divierten mientras te invitan a opinar con ellas sobre temas que nos aquejan e inquietan día a día.  <br/><br/>No te pierdas el espacio creado por mujeres para mujeres… y para hombres también.<br/><br/>Sábado, 10 de la mañana. En Radio TexMex FM… The Radio With No Limits. "},
    {"nombre":" MANUAL DEL VIAJERO", "imagen":"images/programas/manual-viajero.gif", "descripcion":"Viajar sin gastar de más y disfrutando al máximo el destino es posible y sencillo. Escucha los mejores consejos que todo buen viajero debe tomar en cuenta al momento de hacer la maleta y cargar la cámara al cuello.<br/><br/>MANUAL DEL VIAJERO te lleva por los mejores destinos nacionales e internacionales para que sólo te preocupes por decidir dónde será tu siguiente parada.<br/><br/>Sábado, 11:00 horas. Sólo en Radio TexMex FM… The Radio With No Limits. "},
    {"nombre":" DESDE LA BUTACA", "imagen":"images/programas/desde-butaca.gif", "descripcion":"Te acercamos a lo mejor y más reciente del séptimo arte: noticias, premieres, soundtracks, películas clásicas, actores y actrices que se han vuelto leyendas, esto y mucho más, sólo en DESDE LA BUTACA.<br/><br/>Este espacio es tuyo, disfrútalo y nútrelo con tus anécdotas y recuerdos, a través de Radio TexMex FM… The Radio With No Limits.  <br/><br/>Sábado al medio día.   "}

    ]',
 View::POS_HEAD);
$this->registerJs('
    $("#buscar_anunciantes").on("click",function(){
    var texto=$("#Anunciantes_nombre").val();
    var url=$(this).attr("href");
    if (texto.length>0) {
        $.ajax({
            url: url,
            data: {BuscadorAnunciantes:{nombre:texto}},
            success:function(respuesta){$("#Anunciantes_resultados").html(respuesta)}
        });
    }
    return false;
});

$(".btn-programa").on("click",function(e){
    //Carga el contenido del programa 
    $("#programa-imagen").attr("src",programas[$(this).data("pid")].imagen);
    $("#programa-nombre").text(programas[$(this).data("pid")].nombre);
    $("#programa-descripcion").html(programas[$(this).data("pid")].descripcion);
});

    ',View::POS_END)
 ?>



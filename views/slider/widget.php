<?php 
use yii\bootstrap\Carousel;
use yii\helpers\Html;
use app\models\Slider;

$items=[];
foreach (Slider::findAll(['estatus'=>'ALTA']) as $slide) {
	$items[]=[
	'content'=>strlen($slide->url)>5?Html::a($slide->imagenLogo,$slide->url,['class'=>'btn-link']):$slide->imagenLogo,
	'caption'=>Html::tag('h2',$slide->nombre).$slide->descripcion];
}
?>
<?=
Carousel::widget([
	'controls'=>false,
    'items' =>$items
]);
 ?>
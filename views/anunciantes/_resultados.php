
<?php 
use yii\grid\GridView;
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'',
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'nombre',
            'contenido:ntext',
            // 'estatus',
            // 'categorias',
            'imagenLogo:html',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
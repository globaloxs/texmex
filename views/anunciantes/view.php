<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Anunciantes */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Anunciantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anunciantes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Esta usted seguro de eliminar a este anunciante?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'descripcion:ntext',
            'estatus',
            'categorias',
            'logo',
            'imagenLogo:html',
        ],
    ]) ?>

</div>

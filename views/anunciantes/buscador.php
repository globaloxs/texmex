                            <?= 
                            GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'id',
                                'nombre',
                                'descripcion:ntext',
                                'estatus',
                                'categorias',
                                'logo',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
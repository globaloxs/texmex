<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use app\models\Categorias;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Anunciantes */
/* @var $form yii\widgets\ActiveForm */

$categorias=Categorias::find()->all();
?>

<div class="anunciantes-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 200]) ?>

<?= $form->field($model, 'descripcion')->textArea(['rows' => '6']); ?>
<?php 
//  $form->field($model, 'text')->widget(TinyMce::className(), [
//     'options' => ['rows' => 6],
//     'language' => 'es',
//     'clientOptions' => [
//         'plugins' => [
//             "advlist autolink lists link charmap print preview anchor",
//             "searchreplace visualblocks code fullscreen",
//             "insertdatetime media table contextmenu paste"
//         ],
//         'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
//     ]
// ]);
 ?>
    <?= $form->field($model, 'estatus')->dropDownList([ 'ALTA' => 'ALTA', 'BAJA' => 'BAJA', ], []) ?>
    <?= $form->field($model, 'categorias_array')->dropDownList(
    ArrayHelper::map($categorias,'id','nombre'), ['multiple' => true]) ?>

    <?= $form->field($model, 'archivo_imagen')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar cambios', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$this->registerJs("
$('#anunciantes-categorias_array').prop('multiple',true);
    $('#anunciantes-categorias_array').chosen({width: '95%'})");
 ?>


<?php 
use app\models\Categorias;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use kartik\popover\PopoverX;
echo  Html::tag('div',
	Html::textInput('Anunciantes[nombre]','',
		[
		'placeholder'=>'Nombre del anunciante',
		'class'=>'form-control',
		'style'=>'width:70%;display:inline',
		'id'=>'Anunciantes_nombre'
		]).
	Html::a('Buscar',['anunciantes/buscar'],[
		'class'=>'btn btn-info',
		'id'=>'buscar_anunciantes'
		])
	,['class'=>'col-xs-7 col-center']);
echo "<br>";
echo Html::tag('div','',['id'=>'Anunciantes_resultados']);
$listaCatego=Categorias::find()->orderBy('estatus desc')->limit(3)->all();
foreach ($listaCatego as $catego) {
	 // echo '<div>',$catego->nombre,' ' ;
	 echo Html::beginTag('div',array('class'=>'col-xs-4 col-md-4'));
	 echo '<div class="panel panel-default"> <div class="panel-body">';
	 echo Html::beginTag('ul',array('class'=>'nav nav-pills nav-stacked'));
	 $items=[];
	 $items[]=['label'=>$catego->nombre,'class'=>'active'];
	 echo Html::tag('li',Html::a($catego->nombre,'#'),['class'=>'active','role'=>'presentation']);
	 foreach ($catego->anunciantes as $anunciante) {
	 	# Por cada anunciante de cada categoria
	 	$widget= PopoverX::widget([
	 		'header' => $anunciante->nombre,
	 		'placement' => PopoverX::ALIGN_TOP,
	 		'content' =>$anunciante->descripcion,
	 		'toggleButton' => ['label'=>$anunciante->nombre, 'class'=>'btn-link'],
	 		]);
	 	echo Html::tag('li',$widget,['role'=>'presentation']);
	 	// $items[]=['label'=>$anunciante->nombre,'url'=>'#anunciantes'];
	 }
	 echo Html::endTag('ul');
	 echo '</div> </div>';
	 echo Html::endTag('div');
}

 ?>
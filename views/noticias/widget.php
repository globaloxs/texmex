<div class="widget-noticias">
	
<?php 
use yii\helpers\Html;
use app\models\Noticias;
foreach (Noticias::find(['estatus'=>'ALTA'])->orderBy('fecha desc')->all() as $noticia) {
	# por cada noticia en estatus de alta
	echo Html::tag('h4',$noticia->titulo, ['class'=>'text-primary']);
	if (strlen($noticia->imagen)>3) {
		# Si se definio una imagen
		echo Html::img(Yii::getAlias('@web/images/'.$noticia->imagen),['class'=>'img-thumbnail img-noticia']);
	}
	echo Html::tag('small',$noticia->contenido,['class'=>'']);
	echo Html::tag('hr','',['style'=>'border:1px dashed #CCC;margin:5px 0px']);
}
 ?>
</div>
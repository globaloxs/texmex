<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Noticias */

$this->title = $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticias-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Esta usted seguro de eliminar esta noticia?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= 
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'titulo',
            'contenido:ntext',
            'estatus',
            'fecha',
            'imagen',
            ['label'=>'Imagen','value'=>Html::img($model->rutaImagen,['class'=>'img-noticias']), 'format'=>'html']

        ],
    ]) ?>

</div>

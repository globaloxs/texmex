<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'globaloxs',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

    ],
        'modules' => [
            'social' => [
                // the module class
                'class' => 'kartik\social\Module',
         
                // the global settings for the Disqus widget
                'disqus' => [
                    'settings' => ['shortname' => 'DISQUS_SHORTNAME'] // default settings
                ],
         
                // the global settings for the Facebook plugins widget
                'facebook' => [

                    'appId' => '1419350631619033',
                    'secret' => 'Kim010203',

                ],
         
                // the global settings for the Google+ Plugins widget
                'google' => [
                    'clientId' => 'GOOGLE_API_CLIENT_ID',
                    'pageId' => 'GOOGLE_PLUS_PAGE_ID',
                    'profileId' => 'GOOGLE_PLUS_PROFILE_ID',
                ],
         
                // the global settings for the Google Analytics plugin widget
                'googleAnalytics' => [
                    'id' => 'TRACKING_ID',
                    'domain' => 'TRACKING_DOMAIN',
                ],
         
                // the global settings for the Twitter plugin widget
                'twitter' => [
                    'screenName' => '@gizmodo'
                ],
         
                // the global settings for the GitHub plugin widget
                'github' => [
                    'settings' => ['user' => 'GITHUB_USER', 'repo' => 'GITHUB_REPO']
                ],
            ],
            'gii' => 'yii\gii\Module',

            // your other modules
],
    'params' => $params,

];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;

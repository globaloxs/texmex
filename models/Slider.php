<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $url
 * @property string $estatus
 * @property string $imagen
 */
class Slider extends \yii\db\ActiveRecord
{
    public $archivo_imagen;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'estatus' ], 'required'],
            [['descripcion', 'estatus'], 'string'],
            [['nombre'], 'string', 'max' => 200],
            [['archivo_imagen'], 'file'],
            [['url','imagen'], 'string', 'max' => 145]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'url' => 'Url',
            'estatus' => 'Estatus',
            'imagen' => 'Imagen',
        ];
    }
    public function getImagenLogo()
    {
        # Regresa el nombre de la imagen junto con la ruta de acceso url
        return Html::img(Yii::getAlias('@web/images/'.$this->imagen),['class'=>'']);
    }    
}

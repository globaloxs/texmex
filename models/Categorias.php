<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $estatus
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'estatus'], 'required'],
            [['estatus'], 'integer'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'estatus' => 'Estatus',
        ];
    }

    public function getAnunciantes()
    {
        # Devuelve el arreglo de anunciantes que pertenecen a esta categoria
        $findin=new \yii\db\Expression("find_in_set ($this->id,`categorias`)");
        return Anunciantes::find()->where($findin)->all();
    }

}

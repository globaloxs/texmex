<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacto".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $titulo
 * @property string $mensaje
 * @property string $correo
 * @property string $fecha
 */
class Contacto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'mensaje', 'correo', 'fecha'], 'required'],
            [['mensaje'], 'string'],
            [['fecha'], 'safe'],
            [['nombre', 'titulo'], 'string', 'max' => 200],
            [['correo'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'titulo' => 'Titulo',
            'mensaje' => 'Mensaje',
            'correo' => 'Correo',
            'fecha' => 'Fecha',
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "noticias".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $contenido
 * @property string $estatus
 * @property string $fecha
 * @property string $imagen
 */
use yii\web\UploadedFile;

class Noticias extends \yii\db\ActiveRecord
{
    public $img;
    public $archivo_imagen;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'contenido', 'estatus', ], 'required', 'message'=>'El {attribute} no se puede dejar vacio'],
            [['contenido', 'estatus'], 'string'],
            [['fecha','img','imagen'], 'safe'],
            [['archivo_imagen'], 'file'],
            [['titulo'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'contenido' => 'Contenido',
            'estatus' => 'Estatus',
            'fecha' => 'Fecha',
            'imagen' => 'Imagen',
        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->fecha=new Expression('NOW()');
            return true;
        } else {
            return false;
        }
    }

    public function getRutaImagen()
    {
        # Regresa el nombre de la imagen junto con la ruta de acceso url
        return Yii::getAlias('@web/images/'.$this->imagen);
    }
}

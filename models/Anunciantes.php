<?php

namespace app\models;

    use Yii;
    use yii\helpers\Html;
use yii\web\UploadedFile;


/**
 * This is the model class for table "anunciantes".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $estatus
 * @property string $categorias
 * @property string $logo
 */
class Anunciantes extends \yii\db\ActiveRecord
{
    public $categorias_array;
    public $archivo_imagen;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'anunciantes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'descripcion', 'estatus'], 'required', 'message'=>'El {attribute} no se puede dejar vacio'],
            [['descripcion', 'estatus'], 'string'],
            [['logo'], 'file'],
            [['categorias_array','categorias'], 'safe'],
            [['nombre'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'estatus' => 'Estatus',
            'categorias' => 'Categorias',
            'logo' => 'Logo',
        ];
    }

    public function afterFind()
    {
                parent::afterFind();
                $this->categorias_array=explode(',',$this->categorias);

    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
        // ...custom code here...
            if (!empty($this->categorias_array)) {
                # Si se seleccionaron categorias se guardan como un texto plano separado por comas
                $this->categorias=implode(',', $this->categorias_array);
            }
            return true;
        } else {
            return false;
        }    
    }

    public function getImagenLogo()
    {
        # regresa la imagen del logo
        return Html::img(Yii::getAlias('@web/images/'.$this->logo),['class'=>'img-thumbnail']);
    }

    public function getCategorias()
    {
        # regresa un arreglo de categorias dependiendo de los ids separados por comas
        $ids=explode(',', $this->categorias);
        if (!empty($ids)) {
            # Si se le han especificado ids de categorias
            return Categorias::find($ids);
        }else{
            return [];
        }
    }



   /**
     * Devuelve el contenido sin html en los primeros n caracteres
     */
    public function getContenido()
    {
        return substr(strip_tags($this->descripcion),0,250);
    }
}

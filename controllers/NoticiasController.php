<?php

namespace app\controllers;

use Yii;
use app\models\Noticias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * NoticiasController implements the CRUD actions for Noticias model.
 */
class NoticiasController extends Controller
{
    public $layout="admin";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [ 
                'class' => AccessControl::className(), 
                'rules' => [
                        ['roles' => ['?'], 'allow' => false, ],
                        ['roles' => ['@'], 'allow' => true, ],
                    ]
            ]            
        ];
    }

    /**
     * Lists all Noticias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Noticias model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Noticias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Noticias();

        if ($model->load(Yii::$app->request->post())) {
                # Se sube el archivo y se le asigna al atributo
            $model->archivo_imagen = UploadedFile::getInstance($model, 'archivo_imagen');
            if ($model->validate()) {     
                if (!empty($model->archivo_imagen)) {
                    # Si se subio una imagen
                    $nombre=hash('md5',$model->archivo_imagen->baseName) . 
                        '.' . $model->archivo_imagen->extension;
                    $model->archivo_imagen->saveAs('images/' . $nombre );
                    $model->imagen=$nombre;
                }           

            }            
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
                return $this->render('create', [
                'model' => $model,
                ]);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Noticias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
                # Se sube el archivo y se le asigna al atributo
            $model->archivo_imagen = UploadedFile::getInstance($model, 'archivo_imagen');
            // var_dump($model->imagen);
            if ($model->validate()) {     
                if (!empty($model->archivo_imagen)) {
                    # Si se subio una imagen
                    $nombre=hash('md5',$model->archivo_imagen->baseName) . 
                        '.' . $model->archivo_imagen->extension;
                    $model->archivo_imagen->saveAs('images/' . $nombre );
                    $model->imagen=$nombre;
                }           

            }            
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
                return $this->render('update', [
                    'model' => $model,
                    ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                ]);
        }
    }

    /**
     * Deletes an existing Noticias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Noticias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Noticias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Noticias::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionWidget()
    {
        #Muestra el listado de noticias  que se muestra en la pagina frontal
        $model=new Noticias;
        $this->renderPartial('widget',compact('model'));
    }
}

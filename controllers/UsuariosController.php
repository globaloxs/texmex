<?php

namespace app\controllers;

use Yii;
use app\models\Usuarios;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * UsuariosController implements the CRUD actions for Usuarios model.
 */
class UsuariosController extends Controller
{
    public $layout="admin";
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
                ],
            'access' => [ 
                'class' => AccessControl::className(), 
                'rules' => [
                        ['roles' => ['?'], 'allow' => false, ],
                        ['roles' => ['@'], 'allow' => true, ],
                        ['actions'=>['login'], 'roles' => ['?'], 'allow' => true, ],
                        ['actions' => ['logout'], 'roles' => ['@'], 'allow' => true, ]
                    ]
            ]            
        ];
    }

    /**
     * Lists all Usuarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Usuarios::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuarios model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usuarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuarios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Usuarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Usuarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuarios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Renderiza el formulario de logeo.
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) 
            return $this->redirect(['noticias/index']);
        $model=new LoginForm();
        if ($model->load(Yii::$app->request->post()) and $model->login()) {
            # Entra a la validacion de cuenta
            return $this->redirect(['noticias/index']);
        }
        #Rederiza el formulario.
        return $this->render('login',compact('model'));
    }

    public function actionResetear($id)
    {
        # Cambia la contraseña
        $model=$this->findModel($id);
        if ($model->load(Yii::$app->request->post()) ) {
            $model->scenario="reset";
            # Entra a la validacion de cuenta
            if ($model->validate()) {
                # Si las contraseñas coinciden 
                    $model->save();
                    return $this->redirect(['index']);
            }

        }
        return $this->render('_reseteo',compact('model'));       
        #Rederiza el formulario.
    }

    public function actionConfig()
    {
        # Se encarga de cambiar el fondo de la pagina
        if (!empty($_POST)) {
            echo "<div class='alert'> Se subio la imagen </div>";
            $archivo= UploadedFile::getInstanceByName('imagen');
                if (!empty($archivo)) {
                # Si se subio una imagen
                    $archivo->saveAs('images/fondo.jpg'); 
                }
            }
            return $this->render('configuraciones');       

    }
}

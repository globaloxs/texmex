<?php

namespace app\controllers;

use Yii;
use app\models\Anunciantes;
use app\models\BuscadorAnunciantes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * AnunciantesController implements the CRUD actions for Anunciantes model.
 */
class AnunciantesController extends Controller
{
    public $layout="admin";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [ 
                'class' => AccessControl::className(), 
                'rules' => [
                        ['roles' => ['?'], 'allow' => false, ],
                        ['roles' => ['@'], 'allow' => true, ],
                    ]
            ]              
        ];
    }

    /**
     * Lists all Anunciantes models.
     * @return mixed
     */
 public function actionIndex()
    {
        $searchModel = new BuscadorAnunciantes();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionBuscar()
    {
        $searchModel = new BuscadorAnunciantes();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->renderPartial('_resultados', [
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Anunciantes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Anunciantes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Anunciantes();

        if ($model->load(Yii::$app->request->post())) {
                # Se sube el archivo y se le asigna al atributo
            $model->logo = UploadedFile::getInstance($model, 'logo');
            // var_dump($model->logo);
            if ($model->validate()) {                
                if (is_object($model->logo)) {
                    # Si se logra instanciar al modelo de uploadfile
                    $model->logo->saveAs('images/' . $model->logo->baseName . '.' . $model->logo->extension);
                }                  
            }            

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
                return $this->render('create', [
                    'model' => $model,
                    ]);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
                ]);
        }
    }

    /**
     * Updates an existing Anunciantes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->archivo_imagen = UploadedFile::getInstance($model, 'archivo_imagen');
            // var_dump($model->archivo_imagen);
            if ($model->validate()) { 
                if (!empty($model->archivo_imagen)) {
                # Si se subio una imagen
                    $nombre=hash('md5',$model->archivo_imagen->baseName . '.' . $model->archivo_imagen->extension);
                    $model->archivo_imagen->saveAs('images/' . $nombre); 
                    $model->logo=$nombre;
                }
            }               
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
                return $this->render('update', [
                    'model' => $model,
                    ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                ]);
        }
    }

    /**
     * Deletes an existing Anunciantes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Anunciantes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Anunciantes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Anunciantes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

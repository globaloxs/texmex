<?php

namespace app\controllers;

use Yii;
use app\models\Slider;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    public $layout="admin";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest and $action->id!='login'){
            $this->redirect(['site/login']);
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Slider::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slider();

        if ($model->load(Yii::$app->request->post())) {
                # Se sube el archivo y se le asigna al atributo
            $model->archivo_imagen = UploadedFile::getInstance($model, 'archivo_imagen');
            // var_dump($model->archivo_imagen);
            if ($model->validate()) { 
                if (!empty($model->archivo_imagen)) {
                # Si se subio una imagen
                    $nombre=hash('md5',$model->archivo_imagen->baseName . '.' . $model->archivo_imagen->extension);
                    $model->archivo_imagen->saveAs('images/' . $nombre); 
                    $model->imagen=$nombre;
                }
            }            
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
                return $this->render('create', [
                'model' => $model,
                ]);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
                # Se sube el archivo y se le asigna al atributo
            $model->archivo_imagen = UploadedFile::getInstance($model, 'archivo_imagen');
            if ($model->validate()) { 
                if (!empty($model->archivo_imagen)) {
                # Si se subio una imagen
                    $nombre=hash('md5',$model->archivo_imagen->baseName . '.' . $model->archivo_imagen->extension);
                    $model->archivo_imagen->saveAs('images/' . $nombre); 
                    $model->imagen=$nombre;
                }
            }         
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
                return $this->render('update', [
                    'model' => $model,
                    ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                ]);
        }
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
